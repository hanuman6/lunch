import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/login'
import List from '@/components/List'
import Add from '@/components/Add'
import Bookmark from '@/components/Bookmark'
import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'
import 'vue-material/dist/vue-material.min.js'

Vue.use(VueMaterial)
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'login',
      component: Login
    },
    {
      path: '/list',
      name: 'List',
      component: List
    },
    {
      path: '/add',
      name: 'Add',
      component: Add
    },
    {
      path: '/bookmark',
      name: 'Bookmark',
      component: Bookmark
    }
  ]
})
